class Solution {
    public int repeatedStringMatch(String a, String b) {
        int repetitions = 1;
        StringBuilder sb = new StringBuilder(a);
        
        while (sb.length() < b.length()) {
            sb.append(a);
            repetitions++;
        }
        
        if (sb.indexOf(b) != -1) {
            return repetitions;
        }
        
        // If not found, check once more by adding another repetition
        sb.append(a);
        repetitions++;
        
        if (sb.indexOf(b) != -1) {
            return repetitions;
        }
        
        return -1;
    }
}
